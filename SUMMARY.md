# Summary

* [Summary](README.md)
* [Requisites](requisites.md)
* [Setting Up OpenCMIS Server code in IntelliJ](setting-up-opencmis-server-code-in-intellij.md)
* [Running OpenCMIS Server code from IntelliJ](running-opencmis-server-code-from-intellij.md)
* [Setting Up Apache Tomcat in IntelliJ](setting-up-apache-tomcat-in-intellij.md)

