# Running OpenCMIS Server Development Guide code from IntelliJ

Now that you completed successfully the [Setting Up OpenCMIS Server Development Guide code in IntelliJ](/setting-up-opencmis-server-code-in-intellij.md) Chapter, you can run the OpenCMIS Server Development Guide project from the IntelliJ IDEA IDE. To run the project, first you need to create a new configuration. In the right upper corner of the IDE window, click the empty dropdown box as shown in [Figure 9](/assets/09 - Edit Project Configuration.png). From the presented options now choose "Edit Configurations...".

**Figure 9 - Edit Project Configuration**  
![](/assets/09 - Edit Project Configuration.png)

The "Run/Debug Configurations" window should open. In the left upper corner of the window click the "+" icon and choose the option "Tomcat Server" from the list, as shown in [Figure 10](/assets/10 - Add New Configuration.png), proceeded by choosing the option "Local" from the submenu.

**Figure 10 - Add New Configuration**  
![](/assets/10 - Add New Configuration.png)

IntelliJ will then create a new blank Tomcat Server Configuration and present it in the right side of the "Run/Debug Configurations" window. You should change the configuration name from "Unnamed" to something more meaningful like, for example, "opencmis server". The chosen name will be presented to you in the configurations dropdown box in the right upper corner of the IDE window.

The fields which need to be configured are:

* **Application server**

  * Choose an existing Tomcat server already configured in your system. If no Tomcat server is presented to you in the "Application server" dropdown box, please, read the Chapter [Setting Up Apache Tomcat in IntelliJ](/setting-up-apache-tomcat-in-intellij.md) for detailed instructions on how to configure one.

* **JRE**

  * If this setting is not defined, choose a JRE SDK from the ones installed in your system. It is recommended that you use 
    **JRE 1.8**
    although JRE 1.7 will work as well.

* **Before launch: Build, Activate tool window**

  * Make sure you have the "**Build**" option in the list, otherwise the server will not start.

Once you complete the configuration, click the "Apply" button followed by the "OK" button. [Figure 11](/assets/11 - IntelliJ Run Debug Configuration Window.png) illustrates the correct configuration for the project.

**Figure 11 - IntelliJ Run / Debug Configuration Window**  
![](/assets/11 - IntelliJ Run Debug Configuration Window.png)

If you see a "**Warning: No artifacts marked for deployment**" message in the bottom of the "Run/Debug Configurations" window, please click the button "**Fix**" on the right hand side of the window. You will be taken to the "Deployment" tab where you should configure the "**server:war exploded**" artifact to be built on project startup. To do this, click the "+" icon at the bottom of the list, choose "Artifact..." followed by the option "server:war exploded" and click "OK". You should now see the "server:war exploded" artifact both in the "Deploy at the server startup" and "Before launch: Build, Build Artifacts, Activate tool window" lists, as shown by [Figure 12](/assets/12 - Deployment Configuration.png).

**Figure 12 - Deployment Configuration**  
![](/assets/12 - Deployment Configuration.png)

Now that the new configuration is in place, click the green "Play" icon in the right upper corner of the IntelliJ IDEA IDE to run the project. The IDE will open the "Run" panel in the lower part of the window and bootstrap OpenCMIS Server Development Guide server. After the server is up, the IDE will also launch the front-end application and open it in your default browser window. However, if the Apache Tomcat server was freshly installed, a lack of permissions for execution error like the one shown in [Figure 13](/assets/13 - Catalina.sh Permission Denied Error.png) may occur.

**Figure 13 - Catalina.sh Permission Denied Error**  
![](/assets/13 - Catalina.sh Permission Denied Error.png)

Luckily, this error has a simple solution. To solve it, all you have to do is granting the right permissions to the "**catalina.sh**" script to be able to bootstrap the Apache Tomcat server. To grant those permissions, from a terminal window navigate to your apache tomcat instance installation folder. From that location, type the command "**chmod a+x bin/catalina.sh**" followed by the "**enter**" key, as illustrated by [Figure 14](/assets/14 - Catalina.sh Permission Denied Error Solution.png).

**Figure 14 - Catalina.sh Permission Denied Error Solution**  
![](/assets/14 - Catalina.sh Permission Denied Error Solution.png)

You should now be able to bootstrap Tomcat correctly. [Figure 15](/assets/15 - Run CMIS File Bridge Project.png) illustrates the server bootstrap process inside the IntelliJ IDE.

**Figure 15 - Run CMIS File Bridge Project**  
![](/assets/15 - Run CMIS File Bridge Project.png)

If the CMIS File Bridge Server instance started successfully, you should now be seeing the running instance in your browser window, as shown in [Figure 16](/assets/16 - CMIS File Bridge Server.png).

**Figure 16 - CMIS File Bridge Server**  
![](/assets/16 - CMIS File Bridge Server.png)

This concludes the OpenCMIS Server Development Guide project configuration and running inside the IntelliJ IDEA IDE. Additionally, you can read the [Setting Up Apache Tomcat in IntelliJ](/setting-up-apache-tomcat-in-intellij.md) Chapter.

