# Mac OSX / MacOS Development Environment Configuration for Apache Chemistry OpenCMIS Server Development Guide {#mac-osx--macos-development-environment-configuration-guide-for-roda-repository}

This document is a step by step guide for the configuration of a development environment for the Apache Chemistry OpenCMIS Server Development Guide under a Mac OSX / MacOS operating system.

The project consists in a developers guide showing how to build a CMIS server with the Apache Chemistry OpenCMIS Server Framework. For further information, please visit [https://github.com/cmisdocs/ServerDevelopmentGuide](https://github.com/cmisdocs/ServerDevelopmentGuide).

## Author {#markdown-header-author}

**André Rosa**

* [https://bitbucket.org/candrelsrosa](https://bitbucket.org/candrelsrosa)
* [https://github.com/andreros/](https://github.com/andreros/)
* [https://facebook.com/candrelsrosa](https://facebook.com/candrelsrosa)
* [https://twitter.com/candrelsrosa](https://twitter.com/candrelsrosa)

## License {#markdown-header-license}

This guide is released under the [Creative Commons Attribution 4.0 International \(CC BY 4.0\)](https://creativecommons.org/licenses/by/4.0/deed.en) license.

