# Setting Up OpenCMIS Server Development Guide code in IntelliJ

To complete this guide we assume you have a computer with a Mac OSX / MacOS operating system and the IntelliJ IDEA IDE already installed.

To initiate the OpenCMIS Server Development Guide project setup, please, download ServerDevelopmentGuide latest source code from [https://github.com/cmisdocs/](https://github.com/cmisdocs/). There are two different versions of the Server Development Guide. This guide uses version 1 to illustrate the configuration process, however, the configuration process is the same for both versions. [Figure 1](/assets/01 - CMIS Docs Downloads Page.png) shows CMIS Docs GitHub page. To proceed to the download page, please, click the [ServerDevelopmentGuide](https://github.com/cmisdocs/ServerDevelopmentGuide) link.

**Figure 1 - CMIS Docs GitHub Page**  
![](/assets/01 - CMIS Docs Downloads Page.png)

To download the [ServerDevelopmentGuide](https://github.com/cmisdocs/ServerDevelopmentGuide) source code, please, click the "Clone or download" button, identified in [Figure 2](/assets/02 - Download OpenCMIS Server Development Guide.png) with the number 1, and choose the "Download ZIP" option. The code should be downloaded to your computer.

**Figure 2 - Download OpenCMIS server Development Guide**  
![](/assets/02 - Download OpenCMIS Server Development Guide.png)

Extract the ZIP file contents to a folder in your computer. Please, bear in mind the folder you choose will be your project working folder. We suggest placing the OpenCMIS Server Development Guide project folder inside the "/Users/&lt;username&gt;/" folder, as illustrated by [Figure 3](/assets/03 - ServerDevelopmentGuide-master project folder.png).

**Figure 3 - ServerDevelopmentGuide-master Project Folder**  
![](/assets/03 - ServerDevelopmentGuide-master project folder.png)

Open IntelliJ IDEA IDE and from the initial screen, illustrated by [Figure 4](/assets/04 - IntelliJ Open Project.png), choose the "Open" option. Next, select "cmisFileBridge-master" folder inside the "ServerDevelopmentGuide-master" project folder location and click the "Open" button.

**Figure 4 - IntelliJ Open Project**  
![](/assets/04 - IntelliJ Open Project.png)

On opening the project, the IntelliJ IDEA IDE will automatically detect the presence of frameworks and prompt you for its configuration. To access the configuration window and setup the project frameworks, please click the "Configure" link, in blue, as shown by [Figure 5](/assets/05 - IntelliJ Frameworks Detected.png).

**Figure 5 - IntelliJ Frameworks Detected**  
![](/assets/05 - IntelliJ Frameworks Detected.png)

To automatically configure the detected frameworks, select the checkboxes behind each framework presented and click the "OK" button. [Figure 6](/assets/06 - IntelliJ Framework Setup.png) presents the frameworks detected by IntelliJ IDEA IDE for the OpenCMIS Server Development Guide project.

**Figure 6 - IntelliJ Framework Setup**  
![](/assets/06 - IntelliJ Framework Setup.png)

The project will be detected as a Maven project by the IDE and all needed dependencies will also be indexed and automatically downloaded. The dependencies download may take some time, depending on the your internet connection speed. [Figure 7](/assets/07 - IntelliJ Resolving Dependencies.png) shows IntelliJ IDE "Background Tasks" window resolving the project dependencies.

**Figure 7 - IntelliJ Resolving Dependencies**  
![](/assets/07 - IntelliJ Resolving Dependencies.png)

After the frameworks configuration and dependencies resolving, the project is finally set up inside IntelliJ. If the process went well, you should be seeing the "cmisFileBridge-master \[server\]" project file tree like the one shown in [Figure 8](/assets/08 - CMIS File Bridge Project Tree in IntelliJ.png). The "cmisFileBridge-master" project is the server example code for the OpenCMIS Server Development Guide project.

**Figure 8 - CMIS File Bridge Project Tree in IntelliJ**  
![](/assets/08 - CMIS File Bridge Project Tree in IntelliJ.png)

This concludes OpenCMIS Server Development Guide project configuration inside the IntelliJ IDEA IDE. Please, feel free to proceed to the [Running OpenCMIS Server Development Guide code from IntelliJ](/running-opencmis-server-code-from-intellij.md) Chapter.

