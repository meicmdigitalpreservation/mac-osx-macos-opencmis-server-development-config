# Setting Up Apache Tomcat application server in IntelliJ

In this guide we will also demonstrate the configuration of the Apache Tomcat application server in case one is not configured yet in your system. For this purpose we will use **version 9.0.1**.

First, we will start by downloading version 9.0.1 from the Apache Tomcat website. [Figure 17](/assets/17 - Download Apache Tomcat.png) illustrates the Apache Tomcat project download page.

**Figure 17 - Download Apache Tomcat**  
![Download Apache Tomcat](/assets/17 - Download Apache Tomcat.png)

Once we have downloaded the library "apache-tomcat-9.0.1.zip" file, we must extract its contents to a folder. We suggest placing the files folder inside the "/Users/&lt;username&gt;/" folder, as previously done with OpenCMIS Server Development Guide project folder, for reference, as shown by [Figure 18](/18).

**Figure 18 - Apache Tomcat Folder**  
![Apache Tomcat folder](/assets/18 - Apache Tomcat folder.png)

Next, we need to configure IntelliJ to recognize the new instance of the Apache Tomcat application server. To do so, from the IDE "IntelliJ IDEA" menu, we choose the option "Preferences...", as shown by [Figure 19](/assets/19 - IntelliJ Preferences Window.png). Then, in the "Preferences" window search for "**Application Servers**" or browse for that option from the preferences tree located in the left hand side of the window.

**Figure 19 - IntelliJ Preferences Window**  
![IntelliJ Preferences Window](/assets/19 - IntelliJ Preferences Window.png)

In the "Application Servers" list presented in the right hand side of the window click the "+" icon. From the available options choose "Tomcat Server" as illustrated by [Figure 20](/assets/20 - IntelliJ Add Application Server.png).

**Figure 20 - IntelliJ Add Application Server**  
![IntelliJ Add Application Server](/assets/20 - IntelliJ Add Application Server.png)

You will be presented a "Tomcat Server" configuration window. Define the "Tomcat Home" location by clicking the "..." icon and pointing to the Tomcat instance folder in your local hard drive. The remaining fields should be automatically filled. If not, please, define the "Tomcat base directory" by performing the same steps explained before for the "Tomcat Home" configuration. When all fields are filled click the "OK" button. [Figure 21](/assets/21 - IntelliJ Configure Apache Tomcat Home.png) shows the "Tomcat Server" configuration window with the correct values loaded.

**Figure 21 - IntelliJ Configure Apache Tomcat Home**  
![IntelliJ Configure Apache Tomcat Home](/assets/21 - IntelliJ Configure Apache Tomcat Home.png)

If all the configurations were made successfully you should now see the new "Tomcat 9.0.1" application server available in the "Application Servers" list, as shown by [Figure 22](/assets/22 - IntelliJ Apache Tomcat Configuration Finished.png).

**Figure 22 - IntelliJ Apache Tomcat Configuration Finished**  
![IntelliJ Apache Tomcat Configuration Finished](/assets/22 - IntelliJ Apache Tomcat Configuration Finished.png)

This concludes Apache Tomcat application server configuration in IntelliJ and this guide.

