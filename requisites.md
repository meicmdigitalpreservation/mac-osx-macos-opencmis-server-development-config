# Requisites

This guide focuses on the configuration of the development environment for Apache Chemistry OpenCMIS Server Development Guide under a Mac OSX / MacOS environment. Therefore, a computer running any version of Mac OSX or MacOS operating system is a base requirement for the configuration process described by this guide.

The requisites for this guide are:

* A copy of **RODA Repository code**. The latest source code can be downloaded in [https://github.com/keeps/roda](https://github.com/keeps/roda).

* **JetBrains IntelliJ IDEA** IDE. In this guide we will use the **2017 Ultimate** edition. The IDE can be downloaded in [https://www.jetbrains.com/idea/download/\#section=mac](https://www.jetbrains.com/idea/download/#section=mac).

* **Apache Tomcat** application server. In this guide we will use the **version 9.0.1**. Apache Tomcat can be downloaded in [https://tomcat.apache.org/download-90.cgi](https://tomcat.apache.org/download-90.cgi).

Additionally, you can use a Git Client application to checkout Apache Chemistry OpenCMIS Server Development Guide source code from the repository. If you wish to do so, you can use Atlassian Source Tree which can be downloaded in [https://www.sourcetreeapp.com/](https://www.sourcetreeapp.com/).

All the software used in this guide is available for free or under an open source license except for the IntelliJ IDEA IDE. JetBrains offers a free and open source Community edition of the IDE. However, the version used for the development of this guide was the Ultimate edition.

